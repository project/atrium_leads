<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _atrium_leads_content_default_fields() {
  $fields = array();

  // Exported field: field_lead_contacts
  $fields[] = array(
    'field_name' => 'field_lead_contacts',
    'type_name' => 'lead',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          'format' => '4',
          '_error_element' => 'default_value_widget][field_lead_contacts][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Contacts',
      'weight' => 0,
      'description' => 'Enter the contact information for the various people.  ',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_lead_status
  $fields[] = array(
    'field_name' => 'field_lead_status',
    'type_name' => 'lead',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'New
Contacted
In Progress
Closed!
Abandoned',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'New',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Status',
      'weight' => '-2',
      'description' => '',
      'type' => 'optionwidgets_buttons',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_link_url
  $fields[] = array(
    'field_name' => 'field_link_url',
    'type_name' => 'lead',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'url',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'url',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'link',
    'required' => '1',
    'multiple' => '0',
    'module' => 'link',
    'active' => '1',
    'attributes' => array(
      'target' => '_blank',
      'rel' => '',
      'class' => '',
    ),
    'display' => array(
      'url_cutoff' => '80',
    ),
    'url' => 0,
    'title' => 'none',
    'title_value' => '',
    'enable_tokens' => 0,
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'url' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Link',
      'weight' => '-1',
      'description' => '',
      'type' => 'link',
      'module' => 'link',
    ),
  );

  // Translatables
  array(
    t('Contacts'),
    t('Link'),
    t('Status'),
  );

  return $fields;
}

/**
 * Helper to implementation of hook_context_default_contexts().
 */
function _atrium_leads_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'spaces-feature-leads';
  $context->description = 'Simple Business Leads feature.';
  $context->tag = 'Leads';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'lead' => 'lead',
      ),
      'options' => array(
        'node_form' => 1,
      ),
    ),
    'views' => array(
      'values' => array(
        'atrium_leads' => 'atrium_leads',
        'atrium_leads:page_1' => 'atrium_leads:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-atrium_leads-block_2' => array(
          'module' => 'views',
          'delta' => 'atrium_leads-block_2',
          'region' => 'right',
          'weight' => 0,
        ),
        'views-atrium_leads-block_1' => array(
          'module' => 'views',
          'delta' => 'atrium_leads-block_1',
          'region' => 'right',
          'weight' => 1,
        ),
      ),
    ),
    'menu' => 'leads',
  );
  $context->condition_mode = 0;

  $translatables['spaces-feature-leads'] = array(
    t('Leads'),
    t('Simple Business Leads feature.'),
  );

  $export['spaces-feature-leads'] = $context;
  return $export;
}

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _atrium_leads_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  else if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_menu_default_items().
 */
function _atrium_leads_menu_default_items() {
  $items = array();

  $items[] = array(
    'title' => 'Leads',
    'path' => 'leads',
    'weight' => '10',
  );
  // Translatables
  array(
    t('Leads'),
  );


  return $items;
}

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _atrium_leads_user_default_permissions() {
  $permissions = array();

  // Exported permission: create lead content
  $permissions[] = array(
    'name' => 'create lead content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: delete any lead content
  $permissions[] = array(
    'name' => 'delete any lead content',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: delete own lead content
  $permissions[] = array(
    'name' => 'delete own lead content',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: edit any lead content
  $permissions[] = array(
    'name' => 'edit any lead content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'manager',
    ),
  );

  // Exported permission: edit own lead content
  $permissions[] = array(
    'name' => 'edit own lead content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  return $permissions;
}

/**
 * Helper to implementation of hook_strongarm().
 */
function _atrium_leads_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_update_type_lead';
  $strongarm->value = 1;

  $export['atrium_update_type_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_lead';
  $strongarm->value = 0;

  $export['comment_anonymous_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_lead';
  $strongarm->value = '3';

  $export['comment_controls_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_lead';
  $strongarm->value = '4';

  $export['comment_default_mode_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_lead';
  $strongarm->value = '2';

  $export['comment_default_order_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_lead';
  $strongarm->value = '50';

  $export['comment_default_per_page_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:type:lead:driven_props';
  $strongarm->value = array(
    'enabled' => 1,
    'cck' => array(
      'field_lead_status' => array(
        'enabled' => 1,
      ),
      'field_link_url' => array(
        'enabled' => FALSE,
      ),
      'field_lead_contacts' => array(
        'enabled' => 1,
      ),
    ),
    'taxo' => array(
      '1' => array(
        'enabled' => 1,
      ),
    ),
  );

  $export['comment_driven:type:lead:driven_props'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:type:lead:settings';
  $strongarm->value = array(
    'empty_comment' => 1,
    'fieldset:collapsed' => 1,
    'fieldset:title' => 'Business Lead Fields',
    'live_render' => '1',
  );

  $export['comment_driven:type:lead:settings'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_lead';
  $strongarm->value = '1';

  $export['comment_form_location_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_lead';
  $strongarm->value = '2';

  $export['comment_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_lead';
  $strongarm->value = '0';

  $export['comment_preview_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_lead';
  $strongarm->value = '0';

  $export['comment_subject_field_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_upload_images_lead';
  $strongarm->value = 'fileview';

  $export['comment_upload_images_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_upload_lead';
  $strongarm->value = '0';

  $export['comment_upload_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_lead';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-3',
    'revision_information' => '3',
    'comment_settings' => '4',
    'menu' => '-4',
    'book' => '2',
    'path' => '6',
    'attachments' => '5',
    'og_nodeapi' => '1',
  );

  $export['content_extra_weights_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_profile_use_lead';
  $strongarm->value = 0;

  $export['content_profile_use_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_lead';
  $strongarm->value = 1;

  $export['enable_revisions_page_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'form_build_id_lead';
  $strongarm->value = 'form-e3ebf2207db76c709caf8250fa31aa2d';

  $export['form_build_id_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_lead';
  $strongarm->value = array(
    '0' => 'status',
  );

  $export['node_options_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_content_type_lead';
  $strongarm->value = array(
    '0' => 'taxonomy',
    '1' => 'thread',
    '2' => 'nodetype',
    '3' => 'author',
  );

  $export['notifications_content_type_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_node_ui_lead';
  $strongarm->value = array();

  $export['notifications_node_ui_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'og_content_type_usage_lead';
  $strongarm->value = 'group_post_standard';

  $export['og_content_type_usage_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_lead';
  $strongarm->value = 0;

  $export['show_diff_inline_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_lead';
  $strongarm->value = 1;

  $export['show_preview_changes_lead'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_lead';
  $strongarm->value = '1';

  $export['upload_lead'] = $strongarm;
  return $export;
}

<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_leads_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'leads_listing';
  $context->description = 'Simple Business Leads feature.';
  $context->tag = 'Leads';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'lead' => 'lead',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'atrium_leads' => 'atrium_leads',
        'atrium_leads:page_1' => 'atrium_leads:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-atrium_leads-block_2' => array(
          'module' => 'views',
          'delta' => 'atrium_leads-block_2',
          'region' => 'right',
          'weight' => 0,
        ),
        'views-atrium_leads-block_1' => array(
          'module' => 'views',
          'delta' => 'atrium_leads-block_1',
          'region' => 'right',
          'weight' => 1,
        ),
      ),
    ),
    'menu' => 'leads',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Leads');
  t('Simple Business Leads feature.');

  $export['leads_listing'] = $context;
  return $export;
}
